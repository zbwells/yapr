# Yet Another Python Reference

Created with the intention to help University-level Python students study for exams, tests, etc. and to remind students of concepts they may have forgotten.

This is the source code for the book, which consists of multiple Markdown documents, then transformed into an actual ebook-sort of website by the tool mdBook (https://github.com/rust-lang/mdBook).

To view the actual book, look here: http://zbwells.codeberg.page/yapr 

## License

All of the source in this repository is released under the CC0 license, for more information take a look at the [LICENSE](./LICENSE) file.
