# Types

Every variable in Python has a type attached to it. Python is a dynamically typed language, so every variable can change type depending on what data is actually contained in the variable.

* `int`

```python
one = 1
```

* `string`

```python
garbage = "garbage"
```

* `float`

```python
one_point_one = 1.1
```

In actuality, this is more complicated than it seems.

Python does not have data typing in the traditional sense that languages have types; instead, all 'data types' are actually objects of a certain class. 

For instance, `1` will evaluate to an object of `class 'int'`, and "garbage" will evaluate to an object of `class 'string'`. This can be verified with the built-in `type()` function.

```python
print(type(1))
<class 'int'>

print(type("garbage"))
<class 'str'>

print(type({1, 2, 3}))
<class 'set'>

print(type((1, 2, 3,)))
<class 'tuple'>

print(type([1, 2, 3]))
<class 'list'>

print(type({'one': 1}))
<class 'dict'>
```

It is not completely necssary to understand every detail behind how Python actually handles data in order to be proficient with it, but if you would like to investigate further, here are some links:

* [Standard Types](https://docs.python.org/3/library/stdtypes.html)

* [Python Data Model](https://docs.python.org/3/reference/datamodel.html#objects-values-and-types)


