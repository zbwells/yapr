# Built-In Functions

Functions such as `print()` and `str()` are examples of built-in functions. 

There are a number of these functions which are built-in to Python's standard library which may have usage that is not immediately obvious.

`print()` is an extremely common one.

```python
print()
print("Stuff")
```

`print()` can be combined with string formatting to place variables amongst other parts of a string.
```python
x = 5
y = 10
print("The value of x is %i and y is %i" % (x, y))
```

Using the Python interpreter, you can query help pages by using the `help()` function. Using `help()` standalone will output this:

```python
>>> help()

Welcome to Python 3.9's help utility!
```

Writing `print` while in the context of the help utility will output:

```
Help on built-in function print in module builtins:

print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)
    
    Prints the values to a stream, or to sys.stdout by default.
    Optional keyword arguments:
    file:  a file-like object (stream); defaults to the current sys.stdout.
    sep:   string inserted between values, default a space.
    end:   string appended after the last value, default a newline.
    flush: whether to forcibly flush the stream.
```

`help(print)` can be used to display the same help page. This can be done for any function which has a help page-- which includes every built-in function.

Typing `help()` and then `builtins` will show you all of the built-in exceptions, methods, and functions Python has-- which turns out to be thousands of pages worth of them. It it practically impossible to keep even half of Python's builtins in your head at any one time-- which speaks to the importance of documentation in general. This quick-reference does not strive to explain all of them, or even a quarter.

Here is a quick overview of some common built-in functions:

### List

`list()` can be used to transform any kind of sequence into a list object; see section on [Lists](./lists.md).

```python
   tuple1 = (1, 2, 3, 4, 5)
   a = list(tuple1)
```

`a` is now equal to `[1, 2, 3, 4, 5]`, which is a list.

Similarly, `tuple()` can b eused to make any kind of sequence into a tuple.

```
b = tuple(a)
```

And now `b` is equal to `tuple1`.

### Range

`range()` can be used to generate a sequence (iterable object, technically) from a certain number to another number, with explicitly defined steps.

```python
range_var1 = range(1, 11, 2)
```

`list(range_var1)` will contain `[1, 3, 5, 7, 9]`.

```python
range_var2 = range(5, 100)
```

`list(range_var2)` will contain `[5, 99]`.

### Length

`len()` can be used to determine the length of sequences, or any object which has a defined length.

```python
lis = [5, 4, 3, 2, 1]
lislen = len(lis)
```

`lislen` will be 5. `lis`'s final index is 4, because the indexes start from 0.

### Min/Max

`min()` and `max()` can be used to find the greatest or least value in a sequence.

```python
nums = [1, 2, 3, 4, 55, 6]
mn = min(nums)
mx = max(nums)
```

`mn` will be 1, and `mx` will be 55.

### Ord/Chr

`ord()` and `chr()` can be used to find the ordinal and character values of characters and numbers respectively.

```python
A = chr(65)
sixty_five = ord('A')
```

And `hex()` can be used to get the hexadecimal value of an integer.

```python
oh_ex_forty_one = hex(ord('A'))
print("%s is the ascii value of 'A' in hexadecimal." % oh_ex_forty_one)
```

### Reversed

`reversed()` can be used to reverse a sequence, but it will remain a 'reversed object', until otherwise read.

```python
rev = list(reversed("berserk"))
```

`rev` will resolve to `['k', 'r', 'e', 's', 'r', 'e', 'B']`

### Instance

`isinstance()` can be used to check the type of data in a variable.

```python
inst1 = isinstance(34, int)
inst2 = isinstance('a', float)
```

`inst1` will return `True`, and `inst2` and then `False`.

There are a plethora of other built-in functions as aforementioned-- before re-implementing a seemingly simple feature, make sure that Python doesn't provide another way to perform the function already.

See: [https://docs.python.org/3/library/functions.html](https://docs.python.org/3/library/functions.html)





