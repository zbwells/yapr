# Math

Math can be used as part of any expression in Python, in the same way math might be used in mathematics outside of computing. 

```python
twenty = 10 * 2
fourty = twenty * 2
two = fourty / twenty
```

List of mathematical operators:

* `+`
	* Addition
* `-`
	* Subtraction
* `*`
	* Multiplication
* `/`
	* Division
* `**`
	* Exponential
* `%`
	* Modulus (Divide integers and return remainder)
* `//`
	* Floor Division (Divide and round down)

Example of exponential calculation:

```python
one_hundred = 10 ** 2
```

Example of modulus:

```python
one = 13 % 4

# 4 goes into 13 three times, and 1 is the remainder
```

Example of floor division:

```python
four = 9//2

# 9/2 is 4.5, rounded down is 4
```

Python expressions follow standard algebraic order of operations, but best practice is to put parentheses around different operations to make the order more obvious.

```python
thirty_three = (5 + 6) * (2 + 1)
```

If you are unsure of how any of the mathematical operators in Python work, open the interpreter and try as many things as you have to until they make sense.





