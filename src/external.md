# External Resources

Python: [https://www.python.org](https://www.python.org)

Python Library Reference: [https://docs.python.org/3/library/index.html](https://docs.python.org/3/library/index.html)

Python Setup and Usage: [https://docs.python.org/3/using/index.html](https://docs.python.org/3/using/index.html)

Python Glossary: [https://docs.python.org/3/glossary.html](https://docs.python.org/3/glossary.html)

Python Wiki: [https://wiki.python.org/moin/](https://wiki.python.org/moin/)