# Classes

In Python, classes are how data types are defined. This means that defining a Class in Python is synonymous with creating a custom data type.

The purpose and supposed benefits of object orientation and classes is beyond the scope of this reference; suffice to say that if you would like to look into this more, or get a refresher on some OOB premises, [https://python-textbok.readthedocs.io/en/1.0/Classes.html](https://python-textbok.readthedocs.io/en/1.0/Classes.html) may be helpful. 

And for a full list of double underlined built-in class methods, see [https://docs.python.org/3/reference/datamodel.html#basic-customization](https://docs.python.org/3/reference/datamodel.html#basic-customization).

### Basic Class Definition

With that said, here is a class which just contains some data:

```python
class Pet:
   def __init__(self, age, weight, gender):
      self.age = age
      self.weight = weight
      self.gender = gender
```

To access data in the class, you have to create an instance of said class.

```python
pet1 = Pet(5, 50)
print(pet1.weight)
print(pet1.age)
```

```
50
5
```

The `self` portion of the class definition is not a keyword, but rather a self referencing identifier which points to the object itself after it is created.

### Inheritance

If we wanted to include the `Pet` class's parameters in a new class:

```python
class Cat(Pet):

   def animalType(self):
      return "Cat"
```

An instance of this class will inherit the variables listed in Pet, and once created, will also inherit the methods found in the parent (if there were any.)

```python
joe = Cat(5, 30, "Male")
print(joe.animalType())
```

```
Cat
```

There are multiple built-in methods innate to Python classes which can be reimplemented in user-defined classes. These can be used to add extra features to the class, and interface with other built-in functions.

```python
class Dog(Pet):
   def __init__(self, age, weight, gender, name, breed="Unknown")
      # Calling parent class's init method inside new class
      super().__init__(age, weight)
      self.name = name
      self.breed = breed
   
   def __repr__(self):
      return "Dog: %s, %s, age %s" % (self.name, self.gender, self.age)
   
   def __dir__(self):
      return map(str, [self.age, self.weight, self.name, self.gender, self.breed])
```

The built-ins `repr()` and `dir()` can now be used to get a string representation or a directory listing of the class.

```python
bob = Dog(10, 70, "Bob", "Male")
print(repr(bob))
print(dir(bob))
```

```
Dog: Male, Bob, age 10
['10', '70', 'Bob', 'Male', 'Unknown']
```

Be careful when using inheritance— most of the time, it serves only to make things more complicated.
The super() function can be used to access parent-class specific data while within the class definition, and the parent class's __init__ can be called inside the derived class, but both of these things only serve to make using the actual objects a more confusing and tiring venture.

### Another Example

```python
class Beeper:
   def __init__(self):
      self.counter = 0
      self.volume = 0
   
   def beep(self):
      if self.volume > 5:
         print("Beep.")
      if self.volume > 10:
         print("BEEP!")
      else:
         print("beep")
      
      self.counter += 1
```

The values of `counter` and `volume` will change the outcome of `beep()`.

```
alarm = Beeper()
alarm.beep()
alarm.volume = 5
alarm.beep()
print("Alarm went off", alarm.counter, "times.")
```

```
beep
Beep.
beep
Alarm went off 2 times.
```

### Object as an Iterable

```python
class InfiniteSet:
   def __init__(self):
      self.num = 0
   
   # Called to determine what is being iterated through:
   def __iter__(self):
      return self
   
   # Called to determine what the data should look like in sequence:    
   def __next__(self):
      self.num += 1
      return self.num
```

```python
for i in InfiniteSet():
   if i <= 500000:
      print(i, end='r')
   else:
      print()
      break
```

The above will display every number from 0 to 500000 on one line, and then stop at 500000 and continue on to the next line.

```
500000
```

These are only the basics— classes and object orientation are extremely complicated topics, and Python uses them profusely, and in somewhat different ways than languages such as C++ tend to do. For more information about how data is handled in Python, see [https://docs.python.org/3/reference/datamodel.html#objects-values-and-types](https://docs.python.org/3/reference/datamodel.html#objects-values-and-types)
