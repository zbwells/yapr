# Vocabulary

#### For anything not in here, which is most things, try Python's glossary: [https://docs.python.org/3/glossary.html](https://docs.python.org/3/glossary.html)


* Interpreter
	* A program which interprets instructions given either via command line or via external source file(s), and performs commands based on the instructions given. Python is an interpreted programming language. Interpreter programs can be built to parse all sorts of things.
	
* Compiler
	* A program which parses instructions given via text source files, then compiles, assembles, and links the compiled object files together such that they can work as an executable understandable natively by the computer. Each compiler for each compiled language works differently. The GNU C Compiler (GCC) is ubiquitous and is possibly the most common and influential compiler for the C language, along with its counterpart, the GNU C++ Compiler (G++).
	
* Statement
	* A snippet of code that does something, but does not evaluate to a value of any kind.

* Expression
	* A snippet of code that does nothing by itself (is normally paired with expressions), and evaluates to a value.

* Function
	* A block of code which can be called (often with supplied external data, called 'arguments') by another piece of code, which in turn performs operations, sometimes returning data for use elsewhere in the program.
	
* Loop
	* A block of code generally containing two components: a condition, and a body of code which runs a number of times until the condition is met. There are a plethora of different types of loops in most languages, with a few examples being while loops, generic for loops, and numeric for loops. The exact specifications of looping constructs within each programming language vary greatly, and some languages feature no explicit syntax at all, and instead some other feature of the language can be repurposed into a loop.

* Recursion
	* High-level languages which contain functions can often contain functions which call themselves— this can be used to loop in some form.

* Class
	* A language construct which can be used to store operations and data simultaneously, of which the data can be referred to as "program state", and the operations as "methods". By creating an instance of a class, called an "object", programs can be organized in granular chunks which are theoretically easier to organize when working with teams of people. There are varying opinions on whether or not this works as intended.
	
* Object
	* An instance of a class, through which all of the properties and methods in the class can be interacted.
	
* High-level languages
   * Programming languages which are far removed from the actual machine code understood by computers, with the intention for the languages to be more easily understood by humans. C is generally considered the father of modern high-level programming languages, despite originating at the beginning of the 1970s. Python is a high-level programming language.

* Low-level languages
   * Languages which humans nowadays would not actually program in, such as processor assembly, and the actual machine language itself.
