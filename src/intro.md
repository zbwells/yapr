# Intro

#### Welcome!

This is a small quick-reference work which is intended to help students who are taking Python classes and need an easily navicable reference to look at.

It is not intended to teach Python itself, but rather to help students with reminding themselves of certain concepts.

The concepts are also not in any particular order, although concepts which are usually introduced earlier in a Python class are more towards the top of the list.

Simpler concepts may also be left out entirely— for instance, there is no explanation in this reference for simple algebra, or an explanation for what a file is. However, there is a [Vocabulary](vocabulary.md) section.

If you find any errors, or have any suggestions, file an issue report at my Codeberg repository: [https://codeberg.org/zbwells/yapr](https://codeberg.org/zbwells/yapr)

- zbwells 



